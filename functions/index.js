const functions = require("firebase-functions");
const app = require("express")();
const FBAuth = require("./util/fbAuth");

const cors = require("cors");
app.use(cors());

const db = require("./util/admin");

const {
  getAllTweets,
  postOneTweet,
  getTweet,
  commentOnTweet,
  likeTweet,
  unlikeTweet,
  deleteTweet,
  reTweet
} = require("./handlers/tweet");
const {
  signup,
  login,
  addUserDetails,
  getAuthenticatedUser,
  getUserDetails
} = require("./handlers/users");

// Scream routes
app.get("/screams", getAllTweets);
app.post("/scream", FBAuth, postOneTweet);
app.get("/scream/:screamId", getTweet);
app.post("/scream/:screamId/retweet", FBAuth, reTweet);
app.delete("/scream/:screamId", FBAuth, deleteTweet);
app.get("/scream/:screamId/like", FBAuth, likeTweet);
app.get("/scream/:screamId/unlike", FBAuth, unlikeTweet);
app.post("/scream/:screamId/comment", FBAuth, commentOnTweet);

// users routes
app.post("/signup", signup);
app.post("/login", login);
app.post("/user", FBAuth, addUserDetails);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/user/:handle", getUserDetails);

exports.api = functions.region("asia-east2").https.onRequest(app);
